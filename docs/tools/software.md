# 软件

## BI 工具

- Superset：Apache 组织维护的顶流开源，目前 BI 开源届的绝对一哥，超过 47k 的 GitHub Star。  
- Metabase：目前拥有 GitHub Star 超过 29k。Metabase 也是一个完整的 BI 平台，但在设计理念上与 Superset 大不相同。 Metabase 非常注重非技术人员（如产品经理、市场运营人员）在使用这个工具时的体验，让他们能自由地探索数据，回答自己的问题。  
- DataEase：相比前两者，DataEase 是最年轻的 BI 开源工具，GitHub Star 为 6.4k，具备了数据可视化分析工具的核心功能。DataEase 理念上与 Tableau 相近，主打人人可用，并且只专注于数据可视化这个层次。  
