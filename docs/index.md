# 软件项目管理规范

> 包含 AI 辅助创作

## 概述

- [1-00 公司的组织架构](processes/1-00-organization-structure.md)
- [1-01 软件项目管理概述](processes/1-01-management-introduction.md)

## 团队

- [2-00 团队角色](processes/2-00-team-roles.md)
- [2-01 项目经理](processes/2-01-project-manager.md)
- [2-02 产品经理](processes/2-02-product-manager.md)
- [2-03 开发团队](processes/2-03-development-team.md)
- [2-04 测试团队](processes/2-04-quality-team.md)
- [2-05 发布经理](processes/2-05-release-manager.md)

## 流程

- [里程碑计划](processes/milestone-plan-process.md)
- 特性燃起计划(burnup plan)
- 缺陷燃尽计划(burndown plan)
- [项目估算流程](processes/estimation-process.md)
- [Issue 管理流程](processes/issue-management-process.md)

## 规范

- [需求分析规范](guidelines/requirement-analysis-guidelines.md)
- [产品设计规范](guidelines/product-design-guidelines.md)
- [框架设计规范](guidelines/architecture-guidelines.md)
- [代码重构规范](guidelines/refactory-guidelines.md)
- [代码规范](guidelines/coding-guidelines.md)
- [数据库设计规范](guidelines/database-design-guidelines.md)
- [REST API 设计规范](guidelines/rest_guidelines.md)
- [安全规范](guidelines/security_guidelines.md)
- [SQL 编码规范](guidelines/sql_guidelines.md)
- 分支管理规范  
  [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model)

## 管理工具

- [项目管理 - 项目开发工具](tools/project-developing-tools.md)
