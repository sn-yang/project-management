# 开发环境

## 开发环境类型

- 开发环境(development environment)  
  开发环境是开发人员用于开发工作的环境。包含给开发人员使用的数据库、服务和资源等等。  
  有时也可以认为是开发者的本地(development.local)环境。  

- 测试环境(test environment)  
  测试环境是测试人员用于测试工作的环境。包含给测试人员使用的数据库、服务和资源等等。  

- 集结环境(staging environment)  
  集结环境非常接近于生成环境，主要用于在发布产品前的测试工作。  
  比如，可以是最新的产品和生成数据库的组合。  
  有时也称为预发布环境。  

- 生产环境(production environment)  
  生产环境是用户使用的环境。  

## 产品的开发版本

- 内部测试版(alpha)  
  给内部用户使用的测试版。  

- 测试版(beta)  
  公开提供的测试版。  

- 候选发布版(RC: release candidate)  
  是正式版之前的版本。  

- 官方正式版(office release)  
  正式版。倾向于对外、给市场方面使用。  

- 官方正式版(GA: general availability)  
  同正式版。倾向于技术人员使用。  
