import os
import re


def main() -> None:
    path = os.path.abspath(os.path.dirname(__file__))

    # enum_directory(os.path.join(path, '..', "docs"))
    format_file(os.path.join(path, '..', "docs/processes/2-05-release-manager.md"))

def enum_directory(path):
    for root, directories, files in os.walk(path):
        for name in files:
            if name.endswith(".md"):
                format_file(os.path.join(root, name))


def format_file(path: str):
    pattern = "^\s*[^#!].+$"
    pattern_block_begin = "^\s*```[a-zA-Z0-9]+$"
    pattern_block_end = "^\s*```$"
    pattern_math_begin = "^\$\$$"
    pattern_math_end = "^\$\$$"
    data = read_file(path)
    lines = data.split("\n")
    changed = False
    row = -1
    stop = False
    for line in lines:
        row += 1
        if not stop:
            result = re.match(pattern_block_begin, line)
            if result:
                stop = True
                continue
            result = re.match(pattern_math_begin, line)
            if result:
                stop = True
                continue
        if stop:
            result = re.match(pattern_block_end, line)
            if result:
                stop = False
                continue
            result = re.match(pattern_math_end, line)
            if result:
                stop = False
                continue
            continue

        result = re.match(pattern, line)
        if result and len(line) - len(line.rstrip(" ")) != 2:
            # 确保文字行以2个空格结尾
            lines[row] = line.rstrip(" ") + "  "
            changed = True
        

    if changed:
        write_file(path, "\n".join(lines))


def read_file(path: str) -> str:
    with open(path, "r", encoding="utf-8") as file:
        data = file.read()
        return data


def write_file(path: str, data: str) -> None:
    print(path)
    with open(path, "w", encoding="utf-8") as file:
        file.write(data)


if __name__ == "__main__":
    main()
    
    # import os
    # src_dir = os.path.dirname(os.path.abspath(__file__))
    # format_file(os.path.join(src_dir, "test.md"))
