# 编程规范

## AngularJS

[Google: AngularJS Style Guide](https://google.github.io/styleguide/angularjs-google-style.html)  

## C#

[Microsoft: Framework Design Guidelines](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/)  
[Microsoft: C# Coding Conventions](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/coding-conventions)  
[Microsoft: C# Secure Coding Guidelines](https://docs.microsoft.com/en-us/dotnet/standard/security/secure-coding-guidelines)  

## C++

[Google: C++ Style Guide](https://google.github.io/styleguide/cppguide.html)  
[Google: Style Guides](https://google.github.io/styleguide/)  

## Common Lisp

[Google: Common Lisp Style Guide](https://google.github.io/styleguide/lispguide.xml)  

## Database Design

[Database Design Guidelines](database-design-guidelines.md)  

## HTML/CSS

[Google: HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html)  

## Java

[Alibaba Java Coding Guidelines](https://github.com/alibaba/p3c)  
[Google: Java Style Guide](https://google.github.io/styleguide/javaguide.html)  
[Oracle: Code Conventions for the Java TM Programming Language](https://www.oracle.com/technetwork/java/codeconvtoc-136057.html)  
[阿里巴巴 Java 开发手册](https://github.com/alibaba/p3c)  

## JavaScript

[Google: JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html)  

## Objective-C

[Google: Objective-C Style Guide](https://google.github.io/styleguide/objcguide.html)  

## Python

[Google: Python Style Guide](https://google.github.io/styleguide/pyguide.html)  
[PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)  

## R

[Google: R Style Guide](https://google.github.io/styleguide/Rguide.html)  

## Shell

[Google: Shell Style Guide](https://google.github.io/styleguide/shell.xml)  

## Swift

[Google: Swift Style Guide](https://google.github.io/swift/)  

## Vimscript

[Google: Vimscript Style Guide](https://google.github.io/styleguide/vimscriptguide.xml)  

## Visual Basic

[Microsoft: Program Structure and Code Conventions (Visual Basic)](https://docs.microsoft.com/en-us/dotnet/visual-basic/programming-guide/program-structure/program-structure-and-code-conventions)  

## 安全指南

[腾讯安全指南](https://github.com/Tencent/secguide)  

[60 质量工程部开源的《代码安全规则集合》](https://github.com/Qihoo360/safe-rules)  

[Web 开发人员安全须知](https://github.com/FallibleInc/security-guide-for-developers)  

[Web 安全学习笔记](https://github.com/LyleMi/Learn-Web-Hacking)  

[OWASP Top 10 是针对 Web 应用程序的 10 大安全风险提示](https://github.com/OWASP/Top10)  

[API-Security-Checklist](https://github.com/shieldfy/API-Security-Checklist)  

### 安全扫码工具

[nuclei](https://github.com/projectdiscovery/nuclei)  

[gitleaks](https://github.com/zricethezav/gitleaks)  
一款静态应用程序安全测试(SAST)工具。它可以检测 Git 项目中是否包含密码、API Key、token 等敏感信息，还能够轻松整合到 Git Hook 和 GitHub Action，实现提交代码时自动检测，通过告警和阻止 push 等方式，有效地防止敏感信息泄漏。  

[trivy](https://github.com/aquasecurity/trivy)  
一款全面的容器安全扫描工具。当下最流行的开源容器镜像漏洞扫描工具，拥有速度快、精准度高、依赖检测、机密检查、对 CI 友好等特点。它不仅安装简单而且容易上手，仅需一条命令，即可发现镜像存在的安全漏洞。  

[vulhub](https://github.com/vulhub/vulhub)  
一个面向大众的开源漏洞环境集合。无需 Docker 知识，仅需通过一条简单的命令，就能跑起来一个存在某个漏洞的完整应用。使得安全研究人员能够方便地复现与研究漏洞，省去了学习复杂的部署知识、寻找有漏洞的旧版本应用、搭建依赖的服务等麻烦。  
