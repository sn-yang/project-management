#! https://zhuanlan.zhihu.com/p/665077028

# 项目管理 - 团队角色

## 角色

- 项目经理（Project Manager）
- 产品经理（Product Manager）
- 开发经理（Development Manager）
  - 首席开发架构师（Chief Architect）
  - 用户界面设计师（User Interface Designer）
  - 用户体验设计师（User Experience Designer）
  - 软件功能设计师（Software Function Designer）
  - 系统设计师（System Designer）
  - 开发架构师（Architect）
  - 开发工程师（Developer/Software Engineer）
  - 开发运维师（DevOps Engineer）
  - 信息工程师（Information Engineer）
- 测试经理（Test Manager）
  - 首席测试架构师（Chief Test Architect）
  - 测试设计师（Test Designer）
  - 测试架构师（Test Architect）
  - 测试工程师（Test Engineer）
- 发布经理（Release Manager）

> 测试和质量保证的区别？
> 质量保证是指对整个项目管理进行的质量评估。
> 测试是对开发产品的质量评估

## 四辆马车

项目经理负责，项目按照公司规定的流程运作，是团队中的法官。  
产品经理负责，做什么。  
开发团队负责，怎么做。  
测试团队负责，做的怎么样。
